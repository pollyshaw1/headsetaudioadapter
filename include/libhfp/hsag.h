/* -*- C++ -*- */
/*
 * Software Bluetooth Hands-Free Implementation
 *
 * Copyright (C) 2006-2008 Sam Revitch <samr7@cs.washington.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#if !defined(__LIBHFP_HSAG_H__)
#define __LIBHFP_HSAG_H__

#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#include <stdint.h>

#include <libhfp/bt.h>
#include <libhfp/rfcomm.h>
#include <libhfp/soundio.h>
#include <libhfp/soundio-buf.h>
#include <libhfp/hfp.h>


/**
 * @file libhfp/hsag.h
 */

namespace libhfp {

class HsagSession;

/**
 * @brief Service Handler for Hands-Free Profile
 * @ingroup hfp
 *
 *
 * HsagService is a building block of Hands-Free Profile support,
 * implementing single-instance functions including inbound connection
 * handling and automatic reconnection.  As such, one instance of
 * HsagService is created per system.
 *
 * Each attachable Audio Gateway device (typically a cell phone) is
 * represented by a BtDevice object.  HsagService supports the creation
 * of an HsagSession object attached to the BtDevice, to support a Hands
 * Free Profile session with the device.  The life cycle of HsagSession
 * objects is described in the
 * @ref aglifecycle "Audio Gateway Life Cycle" section.
 *
 * @section Hsagserviceoperation Service Operation
 *
 * Clients will typically instantiate one HsagService object, register
 * callbacks, e.g. HsagService::cb_HsagSessionFactory, read known devices
 * from a configuration file, and instantiate an HsagSession object for
 * each with GetSession().  The HsagSession objects can be marked
 * auto-reconnect, via RfcommSession::SetAutoReconnect(), so that they may
 * be automatically connected when they enter radio range.
 *
 * Whenever Hands-Free Profile service is active, HsagService maintains
 * a listening socket for inbound HFP service level connections.  Devices
 * not previously known to HsagService may initiate connections, and will
 * have HsagSession objects created for them.  The HsagService client may
 * control access by:
 * - Refusing to create HsagSession objects for unknown Bluetooth addresses,
 * using HsagService::cb_HsagSessionFactory
 * - Forcing disconnections when devices initiate connections and their
 * HsagSession objects enter the Connecting state, using
 * HsagSession::cb_NotifyDeviceConnection
 * - Requiring authentication, using SetSecMode().
 *
 * Authentication is managed at the system level and is beyond the scope
 * of HsagService or HsagSession other than imposing a requirement that
 * system authentication be upheld.  Additionally, Bluetooth HFP 1.5 does
 * not mandate any level of security on its RFCOMM connections.
 */
class HsagService : public HfpService {
	friend class HsagSession;

protected:
    virtual bool SdpRegister(ErrorInfo *error);
	virtual RfcommSession *SessionFactory(BtDevice *devp);


public:
	HsagService(int caps = 63);
	virtual ~HsagService();
/**
	 * @brief Factory for HfpSession objects, implemented as a callback
	 *
	 * Clients of HfpService may use this callback to construct
	 * derivatives of HfpSession with additional functionality, or
	 * to pre-register callbacks before any of the object's methods
	 * are invoked.
	 *
	 * This callback specifically violates the
	 * @ref callbacks "rule of not invoking callbacks in a nested fashion from client method calls,"
	 * as it may be called nested from Connect() and GetDevice() with
	 * create=true.
	 * Clients choosing to receive this callback may not make additional
	 * HfpService / HfpSession method calls aside from constructors
	 * and DefaultSessionFactory().
	 *
	 * @param baddr_t& Bluetooth address of the device to be represented.
	 * @return Newly created HfpSession object representing the device,
	 * or NULL on failure.
	 */
	Callback<HfpSession*, BtDevice*>	cb_HfpSessionFactory;

	/**
	 * @brief Default factory method for HfpSession objects
	 *
	 * Clients wishing to override HfpService::cb_HfpSessionFactory
	 * may use this method to construct base HfpSession objects.
	 */
	virtual HfpSession *DefaultSessionFactory(BtDevice *);


};

/**
 * @brief Session object for Hands-Free Profile
 * @ingroup hfp
 *
 * This class represents a Hands Free Profile session and connection to
 * a remote audio gateway device (typically a cell phone).  An HFP session
 * through this object can allow the audio gateway to use a speakerphone
 * facility provided by the local system.  It supports numerous use cases:
 * - Interrogating the supported audio gateway features of the device
 * - Disseminating notifications from the device, e.g. signal strength
 * - Sending commands to the device, e.g. dial number, hangup
 * - Streaming audio to and from the device
 *
 * @section aglifecycle Life Cycle
 *
 * The associated HsagService object manages the life cycle of HsagSession
 * objects.  The HsagService keeps an index of HsagSession objects, and
 * ensures that at most one exists per attached BtDevice device object.
 * HsagSession objects may be instantiated through one of three paths:
 *
 * - Through HsagService::GetSession(), with @c create = @c true.  This
 * method requires a reference to an existing BtDevice object, or other
 * explicit knowledge of the Bluetooth address of the target device.
 * This method will also only instantiate a new HsagSession object if
 * one doesn't already exist for the associated BtDevice.
 * - Through HsagService::Connect().  This method is similar to
 * HsagService::GetSession() above, but also initiates an outbound
 * connection to the device.
 * - As part of an inbound connection, HsagService will automatically
 * instantiate an HsagSession for the device if one does not already exist.
 *
 * HsagSession objects are reference-counted
 * @ref managed "Bluetooth managed objects," and are always destroyed
 * under circumstances:
 * - Client reference count drops to zero.  Put().
 * - The HsagSession is in the Disconnected state.  See IsConnected(),
 * IsConnecting().
 * - Auto-reconnect is not enabled.  See RfcommSession::SetAutoReconnect().
 * - As with all @ref managed "managed objects," the actual destruction
 * is performed in the context of a timer event.
 *
 * The HsagSession class is designed so that clients may interact with it
 * without defining derived classes.  All notifications provided by
 * HsagSession are performed through Callback callback objects.
 *
 * Clients may override the instantiation path for HsagSession objects by
 * registering their own factory method to HsagService::cb_HsagSessionFactory.
 * As part of a specialized factory, clients may use the default factory
 * method, HsagService::DefaultSessionFactory().  A specialized factory
 * may be used to:
 * - Register all relevant callbacks with the HsagSession object from a
 * single path, and before any may be invoked.
 * - Instantiate a class derived from HsagSession with additional
 * functionality.  This is recommended only as a last resort.
 *
 * @section hfpscallbacks Callbacks
 *
 * The HfpSession object provides numerous callbacks, and uses the
 * @ref callbacks "Bluetooth module rules for non-nesting."
 *
 * @section hfpsstate State
 *
 * The session may be in one of three states:
 * - Disconnected: ! IsConnecting() && ! IsConnected()
 * - Connecting: IsConnecting()
 * - Connected: IsConnected()
 *
 * When Disconnected or Connecting, none of the feature inquiries are
 * meaningful, no state indicator and call state notifications will
 * be delivered, the audio connection must be Disconnected, and no
 * commands may be issued on the device.
 *
 * Once connected, the device will accept commands, state indicator
 * notifications will be delivered and certain values retained, and the
 * voice audio channel may be connected.
 */
class HsagSession : public HfpSession {
	friend class HsagService;
	friend class AtCommand;

private:
	//virtual void NotifyConnectionState(ErrorInfo *async_error);
protected:
	//void Timeout(TimerNotifier *notp);
	/* Timeout for completion of the topmost command */
	//void CommandTimeout(TimerNotifier *notp);
	//void ClipTimeout(TimerNotifier *notp);
	/* Method reimplemented from RfcommSession */
	virtual void NotifyConnectionState(ErrorInfo *async_error);

    virtual void ScoConnectNotify(SocketNotifier *notp, int fh);



public:
    HsagSession(HsagService *svcp, BtDevice *devp);
};


} /* namespace libhfp */
#endif /* !defined(__LIBHFP_HSAG_H__) */
