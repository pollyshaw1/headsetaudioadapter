/*
 * Software Bluetooth Hands-Free Implementation
 *
 * Copyright (C) 2006-2008 Sam Revitch <samr7@cs.washington.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * This package is specific to Linux and the Qualcomm BlueZ Bluetooth
 * stack for Linux.  It is not specific to any GUI or application
 * framework, and can be adapted to most by creating an appropriate
 * DispatchInterface class.
 */

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <fcntl.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#include <bluetooth/sco.h>
#include <bluetooth/rfcomm.h>

#include <libhfp/hsag.h>

namespace libhfp {


HsagService::
HsagService(int caps)
	: HfpService(caps)
{
    m_search_svclass_id = HEADSET_SVCLASS_ID;
}

HsagService::
~HsagService()
{

}


/*
 * This function, along with the majority of sdp_lib, is ridiculous.
 * It doesn't have to be this hard, guys!
 */
bool HsagService::
SdpRegister(ErrorInfo *error)
{
	uuid_t root_uuid, hfsc_uuid, gasc_uuid;
	sdp_list_t *root_list = 0;
	sdp_profile_desc_t hfp_pdesc;
	sdp_record_t *svcrec;
	uint16_t caps;

	if (!(svcrec = sdp_record_alloc()))
		goto nomem;

	/* No failure reporting path here! */
	sdp_set_info_attr(svcrec,
			  GetServiceName(),
			  NULL,
			  GetServiceDesc());

	sdp_uuid16_create(&hfsc_uuid, HEADSET_AGW_SVCLASS_ID);
	if (!(root_list = sdp_list_append(0, &hfsc_uuid)))
		goto nomem;
	sdp_uuid16_create(&gasc_uuid, GENERIC_AUDIO_SVCLASS_ID);
	if (!(root_list = sdp_list_append(root_list, &gasc_uuid)))
		goto nomem;
	if (sdp_set_service_classes(svcrec, root_list) < 0)
		goto nomem;

	sdp_list_free(root_list, 0);
	root_list = 0;

	sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
	if (!(root_list = sdp_list_append(NULL, &root_uuid)))
		goto nomem;
	if (sdp_set_browse_groups(svcrec, root_list) < 0)
		goto nomem;

	sdp_list_free(root_list, 0);
	root_list = 0;

	if (!RfcommSdpSetAccessProto(svcrec, RfcommGetListenChannel()))
		goto nomem;

	// Profiles
	sdp_uuid16_create(&hfp_pdesc.uuid, HEADSET_PROFILE_ID);
	hfp_pdesc.version = 0x0105;
	if (!(root_list = sdp_list_append(NULL, &hfp_pdesc)))
		goto nomem;
	if (sdp_set_profile_descs(svcrec, root_list) < 0)
		goto nomem;

	sdp_list_free(root_list, 0);
	root_list = 0;

	/* Add one last required attribute */
	caps = m_brsf_my_caps & 0x1f;
	if (sdp_attr_add_new(svcrec, SDP_ATTR_SUPPORTED_FEATURES,
			     SDP_UINT16, &caps) < 0)
		goto nomem;

	if (!GetHub()->SdpRecordRegister(svcrec, error))
		goto failed;

	m_sdp_rec = svcrec;
	return true;

nomem:
	if (error)
		error->SetNoMem();

failed:
	if (root_list)
		sdp_list_free(root_list, 0);
	if (svcrec)
		sdp_record_free(svcrec);
	return false;
}

RfcommSession * HsagService::
SessionFactory(BtDevice *devp)
{
	HsagSession *sessp;

	assert(GetHub());
	assert(!FindSession(devp));

	if (cb_HfpSessionFactory.Registered())
		sessp = cb_HfpSessionFactory(devp);
	else
		sessp = DefaultSessionFactory(devp);

	return sessp;
}

HfpSession *HsagService::
DefaultSessionFactory(BtDevice *devp)
{
	return new HsagSession(this, devp);
}


HsagSession::
HsagSession(HsagService *svcp, BtDevice *devp)
	: HfpSession(svcp, devp)
{
}

void HsagSession::
NotifyConnectionState(ErrorInfo *async_error)
{
	ErrorInfo local_error;

	if (IsRfcommConnecting()) {
		assert(m_conn_state == BTS_Disconnected);
		m_conn_state = BTS_RfcommConnecting;
		assert(!async_error);
		if (cb_NotifyConnection.Registered())
			cb_NotifyConnection(this, 0);
	}

	else if (IsRfcommConnected()) {
		/*
		 * BTS_Disconnected would indicate an inbound connection.
		 * BTS_RfcommConnecting would indicate outbound.
		 */
		assert(!async_error);


		//assert((m_conn_state == BTS_RfcommConnecting) ||
		//       (m_conn_state == BTS_Disconnected));

		m_conn_state = BTS_Connected;

		if (!m_timer) {
			m_timer = GetDi()->NewTimer();
			if (m_timer)
			if (m_timer)
				m_timer->Register((HfpSession*)this, &HsagSession::Timeout);
		}
		if (!m_clip_timer) {
			m_clip_timer = GetDi()->NewTimer();
			if (m_clip_timer)
				m_clip_timer->Register((HfpSession*)this,
						&HsagSession::ClipTimeout);
		}
		if (!m_command_timer) {
			m_command_timer = GetDi()->NewTimer();
			if (m_command_timer)
				m_command_timer->Register((HfpSession*)this,
						&HsagSession::CommandTimeout);
		}

		if (!m_timer || !m_clip_timer || !m_command_timer) {
			local_error.SetNoMem();
			__Disconnect(&local_error, false);
		}

/*		if (IsRfcommConnected() && !HsagHandshake(&local_error)) {
			__Disconnect(&local_error, false);
		}*/

        /* stuff about connecting SCO here */

        if (IsRfcommConnected() && !SndOpen(true, true, &local_error)) {
            __Disconnect(&local_error, false);
        }



		if (IsRfcommConnected() &&
		    IsConnectionRemoteInitiated() &&
		    cb_NotifyConnection.Registered())
			cb_NotifyConnection(this, 0);


	}

	else {
		/* This is all handled by __Disconnect */
		assert(m_conn_state == BTS_Disconnected);
		assert(!IsConnectingAudio());
		assert(!IsConnectedAudio());
		UpdateCallSetup(0);
	}

	if (async_error && cb_NotifyConnection.Registered())
		cb_NotifyConnection(this, async_error);
}


void HsagSession::
ScoConnectNotify(SocketNotifier *notp, int fh)
{
	int sockerr;
	socklen_t sl;
	ErrorInfo error;

	assert(m_sco_state == BVS_SocketConnecting);

	if (notp) {
		assert(notp == m_sco_not);
		delete m_sco_not;
		m_sco_not = 0;
	}

	m_sco_state = BVS_Connected;
	assert(m_sco_not == 0);

	/* Check for a connect error */
	sl = sizeof(sockerr);
	if (getsockopt(m_sco_sock, SOL_SOCKET, SO_ERROR,
		       &sockerr, &sl) < 0) {
		GetDi()->LogWarn(&error,
				 LIBHFP_ERROR_SUBSYS_BT,
				 LIBHFP_ERROR_BT_SYSCALL,
				 "Retrieve status of SCO connect: %s",
				 strerror(errno));
		__DisconnectSco(true, true, false, error);
		return;
	}

	if (sockerr) {
		GetDi()->LogWarn(&error,
				 LIBHFP_ERROR_SUBSYS_BT,
				 LIBHFP_ERROR_BT_SYSCALL,
				 "SCO connect: %s", strerror(sockerr));
		__DisconnectSco(true, true, false, error);
		return;
	}

	/* Retrieve the connection parameters */
	if (!ScoGetParams(m_sco_sock, &error)) {
		/* Both callbacks synchronous */
		__DisconnectSco(true, true, false, error);
		return;
	}

	BufOpen(m_sco_packet_samps, 2);



	if (cb_NotifyAudioConnection.Registered())
		cb_NotifyAudioConnection(this, 0);
}



} /* namespace libhfp */
